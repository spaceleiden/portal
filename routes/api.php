<?php

declare(strict_types=1);

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\Admin\ActivityController as AdminActivityController;
use App\Http\Controllers\Admin\ArchiveController as AdminArchiveController;
use App\Http\Controllers\Admin\NewsController as AdminNewsController;
use App\Http\Controllers\Admin\RegistrationController as AdminRegistrationController;
use App\Http\Controllers\Admin\SettingsController as AdminSettingsController;
use App\Http\Controllers\Admin\UsersController as AdminUsersController;
use App\Http\Controllers\Admin\DonationController as AdminDonationController;
use App\Http\Controllers\Admin\PaymentController as AdminPaymentController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\RobotController as AdminRobotController;
use App\Http\Controllers\RobotController as RobotController;
use App\Http\Controllers\ArchiveController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\DonationController;
use App\Http\Controllers\FinanceController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\SocialmediaController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ToolshareitemController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Public API calls.
 */
Route::group(['prefix' => '/public', 'middleware' => 'cors'], function () {
    Route::get('/status', [StatusController::class, 'index']);
    Route::get('/status.json', [StatusController::class, 'index']);
});

/**
 * Authentication related API calls.
 *
 */
Route::group(['prefix' => '/auth'], function() {

    // Login area auth actions.
    Route::post('/signup', [UserController::class, 'signup'])->name('signup');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/forgot', [AuthController::class, 'forgot'])->name('forgot');
    Route::post('/reset', [AuthController::class, 'reset'])->name('reset');
});

/**
 * Payment related API calls.
 *
 */
Route::group(['prefix' => '/payments'], function() {

    // Mollie callbacks and webhooks.
    Route::get('/callback', [FinanceController::class, 'callback'])->name('callback');
    Route::post('/webhook', [FinanceController::class, 'webhook'])->name('webhook');
    Route::post('/donate/webhook', [DonationController::class, 'webhookDonationPayment']);

    // Call on signup page to display correct subscription amount.
    Route::get('/subscription/default', [FinanceController::class, 'getDefaultSubscription']);

    // Authenticated User calls.
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('/subscription/cancel', [FinanceController::class, 'cancel'])->name('cancel');
        Route::get('/subscription/reactivate', [FinanceController::class, 'reactivate'])->name('reactivate');
        Route::get('/mandate/create', [UserController::class, 'createNewMandatePayment'])->name('createNewMandatePayment');
    });

    // Authenticated Member calls.
    Route::group(['middleware' => 'auth:sanctum', 'role:member'], function() {

        // Donations
        Route::get('/donate/create/{amount}/{add_to_balance}', [DonationController::class, 'createOneTimeMemberDonation']);
        Route::post('/donate/recurring/create', [DonationController::class, 'createRecurringMemberDonation']);
        Route::post('/donate/recurring/cancel', [DonationController::class, 'cancelRecurringMemberDonation']);
        Route::get('/donate/callback', [DonationController::class, 'callbackDonationPayment']);

        // Payments
        Route::get('/balance', [PaymentController::class], 'balance');

        // NOTE: for now users are not allowed to create payments themselves.
        // Route::get('/create', [PaymentController::class, 'create']);
    });

    // Anonymous donations
    // Route::get('/donation/create/{amount}', [DonationController::class, 'createOneTimeAnonymousDonation']);

});

/**
 * Robot API calls.
 */
Route::group(['prefix' => '/robot', 'middleware' => 'robot'], function() {

    // Check if we know the given card uid.
    // Return the name and balance of member if found.
    Route::get('/card/{card_uid}', [RobotController::class, 'get'])->name('robot_check_card');

    // Generate a link code for the given card uid.
    Route::get('/link_code/{card_uid}', [RobotController::class, 'create_link_code'])->name('robot_create_link_code');

    // List available products to purchase.
    Route::get('/products', [ProductController::class, 'index'])->name('robot_list_products');

    // Register a purchase for a member with user_id.
    Route::post('/payments/create', [PaymentController::class, 'create'])->name('robot_create_payment');
});

/**
 * User area API calls.
 *
 */
Route::group(['prefix' => '/me', 'middleware' => 'auth:sanctum'], function() {
    Route::get('/finance', [UserController::class, 'finance'])->name('finance');
    Route::get('/payments', [UserController::class, 'payments'])->name('payments');
    Route::get('/local_payments', [PaymentController::class, 'index'])->name('local_payments');
    Route::get('/', [UserController::class, 'me'])->name('me');
    Route::patch('/', [UserController::class, 'update'])->name('update');
    Route::get('/card/register/{link_code}', [UserController::class, 'registerCard'])->name('register_card');
    Route::delete('/card', [UserController::class, 'removeCard'])->name('remove_card');
});

/**
 * Member area API calls.
 *
 */
Route::group(['middleware' => 'auth:sanctum', 'role:member'], function(){
    Route::get('/activities', [ActivityController::class, 'index']);
    Route::post('/registrations', [RegistrationController::class, 'store']);
    Route::delete('/registrations', [RegistrationController::class, 'destroy']);
    Route::get('/news/internal', [NewsController::class, 'index']);
    Route::get('/news/external', [NewsController::class, 'blogposts']);
    Route::get('/socialmedia', [SocialmediaController::class, 'index']);

    Route::group(['prefix' => '/archive'], function(){
        Route::get('/list/{path?}', [ArchiveController::class, 'list'])->where('path', '(.*)');
        Route::get('/download/{path}', [ArchiveController::class, 'get'])->where('path', '(.*)');
    });

    Route::group(['prefix' => '/toolshareitem'], function() {
        Route::get('/', [ToolshareitemController::class, 'index']);
        Route::post('/', [ToolshareitemController::class, 'store']);
        Route::patch('/{id}', [ToolshareitemController::class, 'update']);
        Route::delete('/{id}', [ToolshareitemController::class, 'destroy']);
    });

});

/**
 * Admin area API calls.
 *
 */
Route::group(['prefix' => '/admin', 'middleware' => ['auth:sanctum', 'role:admin']], function() {

    // Activities
    Route::get('/activities', [AdminActivityController::class, 'index']);
    Route::post('/activities', [AdminActivityController::class, 'store']);
    Route::patch('/activities/{activity}', [AdminActivityController::class, 'update']);
    Route::delete('/activities/{activity}', [AdminActivityController::class, 'remove']);

    // Registrations
    Route::get('/registrations', [AdminActivityController::class, 'registrationCount']);
    Route::post('/registrations', [AdminRegistrationController::class, 'store']);
    Route::delete('/registrations/{registration}', [AdminRegistrationController::class, 'remove']);

    // News
    Route::post('/news/internal', [AdminNewsController::class, 'store']);
    Route::patch('/news/internal/{news}', [AdminNewsController::class, 'store']);
    Route::delete('/news/internal/{news}', [AdminNewsController::class, 'remove']);

    // Users
    Route::get('/users', [AdminUsersController::class, 'index']);
    Route::get('/users/birthdays', [AdminUsersController::class, 'birthdays']);
    Route::get('/users/{user_id}/finance', [AdminUsersController::class, 'finance']);
    Route::get('/users/{user_id}/finance/payments', [AdminUsersController::class, 'payments']);
    Route::get('/users/{user_id}/registrations', [AdminRegistrationController::class, 'getForUser']);

    // Settings
    Route::get('/settings', [AdminSettingsController::class, 'index']);
    Route::patch('/settings', [AdminSettingsController::class, 'update']);

    // Archive
    Route::group(['prefix' => '/archive'], function () {
        Route::post('/file/{path?}', [AdminArchiveController::class, 'createFile'])->where('path', '(.*)');
        Route::delete('/file/{path}', [AdminArchiveController::class, 'deleteFile'])->where('path', '(.*)');
        Route::post('/folder/{path}', [AdminArchiveController::class, 'createFolder'])->where('path', '(.*)');
        Route::delete('/folder/{path}', [AdminArchiveController::class, 'deleteFolder'])->where('path', '(.*)');
    });

    // Donations
    Route::group(['prefix' => '/donations'], function () {
        Route::get('/', [AdminDonationController::class, 'index']);
        Route::patch('/{donation}', [AdminDonationController::class, 'update']);
    });

    // Payments
    Route::group(['prefix' => '/payments'], function () {
        Route::get('/', [AdminPaymentController::class, 'index']);
        Route::post('/create', [AdminPaymentController::class, 'create']);
        Route::delete('/{payment}', [AdminPaymentController::class, 'delete']);
    });

    // Products
    Route::group(['prefix' => '/products'], function () {
        Route::get('/', [AdminProductController::class, 'index']);
        Route::post('/create', [AdminProductController::class, 'create']);
        Route::patch('/{product}', [AdminProductController::class, 'update']);
        Route::delete('/{product}', [AdminProductController::class, 'delete']);
    });

    // Robots
    Route::group(['prefix' => '/robots'], function () {
        Route::get('/', [AdminRobotController::class, 'index']);
        Route::post('/create', [AdminRobotController::class, 'create']);
        Route::patch('/{robot}', [AdminRobotController::class, 'update']);
        Route::delete('/{robot}', [AdminRobotController::class, 'delete']);
    });
});
