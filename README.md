<h1><img src="./resources/img/logo.png" height="28"> Portal</h1>

Portal is a [Laravel](https://laravel.com/docs) application for members of [the hackerspace and makerspace in Leiden](https://spaceleiden.nl).

It's features currently include:
- Member administration;
- Subscription payment integration with Mollie's API;
- Calendar and news;
- Activity registration.

Suggestions, feedback and improvements are welcome. Please [open a new issue on GitLab](https://gitlab.com/spaceleiden/portal/-/issues) to start collaborating.

---

## 1. Local development

To run Portal locally for development purposes, you'll first have to install [docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/), then run the following commands to setup a Laravel docker environment:

```bash
# clone repository
git clone git@gitlab.com:spaceleiden/portal.git

# install composer dependencies using composer
docker run --rm --interactive --tty --volume $PWD:/app composer composer install

# npm update
docker run --rm --interactive --tty --volume $PWD:/app -w /app node npm audit fix

# prepare .env
cp .env.example .env

# run sail
./vendor/bin/sail up
./vendor/bin/sail php artisan key:generate
./vendor/bin/sail php artisan migrate:fresh
./vendor/bin/sail php artisan db:seed

# use a second shell to manage npm
./vendor/bin/sail npm install # or npm install locally if this fails
./vendor/bin/sail npm run watch # or npm run dev
```

Go to http://localhost and you should have the app running.

You can login into the admin account with the following credentials:
- Username: admin@spaceleiden.nl
- Password: password


The Docker stack consists out of four containers:
1. `backend` (or `laravel.test` on development)
2. `database` containing a mysql or mariadb image
3. `queue` containing a Laravel queue worker
4. `scheduler` containing a Laravel scheduler worker

Frontend Vue.js and backend PHP Laravel development live inside the `resources/js` folder and `app` folder respectively.

## 2. Programming handles
This chapter gives some programming handles, divided in:

1. Code style
2. Backend
3. Frontend
4. Permissions
5. Icons

### 2.1 Code style
Our Laravel PHP codebase adheres to the [PSR-12](https://www.php-fig.org/psr/psr-12/)
coding standard. We use the [ecs](https://github.com/symplify/easy-coding-standard) linter.

Our JavaScript Vue codebase adheres to the [standard](https://standardjs.com/)
coding standard. We use the [eslint](https://eslint.org/) [vue](https://eslint.vuejs.org/) linter.

When developing locally, you can easily lint your current codebase with the
following sail commands:

```shell
# run linter for php
./vendor/bin/sail composer lint:fix

# run linter for js / vue
./vendor/bin/sail npm run fix
```

### 2.2 Backend
Looking at the backend authenticated routes should require `auth:sanctum`.

Whenever you return data from a controller, you should use one of the following macros:
```php
# Success
return response()->success(string $message, array $data);

# Error with 500 status code
return response()->error(string $message, array $data);

# Error
return response()->statusError(int $status, string $message, array $data);

```

### 2.3 Frontend
The frontend uses Vue. For requests, we currently use a library called Axios. The app is
configured to first call /api/me. If the client is unauthenticated, the API will return
a HTTP 401 error. This will cause the frontend app to show the login form.

Axios is configured to make calls with the right authorization headers once the user is logged in.
This means that during programming behind the authorization, you won't have to think about it.

You can request data from the api by using axios within a Vue component in the following way:
```javascript
this.axios.get('/api/super/secret/stuff/behind/auth')
    .then(response => {
        // Do amazing stuff with the super secret data here
    }).catch(error => {
        // Either do specific things with the error here or:
        EventBus.$emit('snackbar', error)

        //IMPORTANT: Do note that the event bus requires you to at the top of your component:
        import {EventBus} from "./plugins/events-bus";
    })
```

### 2.4 Permissions
We use [spatie](https://github.com/spatie/laravel-permission) permissions to manage our roles. Currently, we have 2 roles:
- `member`
- `admin`

We also have an implicit state for users without any roles. Which is basically anyone with an account but without having paid for anything.

#### Backend permissions
To give someone a role you can call a method on any user object:
```php
User $user = request()->user();

// This promotes a user to admin immediately
$user->assignRole('admin')

// This demotes a user from admin to non admin
$user->removeRole('admin')
```

Users can have multiple roles, for example: everyone on the board will have the `admin` role and the `member` role.

Routes can be guarded using middleware like `role:admin` or `role:member`:
```php
Route::group(['prefix' => '/admin', 'middleware' => ['auth:sanctum', 'role:admin']], function() {
    // Juicy calls
}
```

#### Frontend permissions
There is a method called `hasRole()` defined in `App.vue`. This can be used to check
whether a user has a certain role or not. It returns true if the user has the role and
false if it doesn't. Example:
```html
<router-link v-if="hasRole('admin')" :to="{name: 'Admin'}">
  <!--// Juicy Link //-->
</router-link>
```

This only shows the router link if the current user has the admin role.
We only have to hide things in the frontend, actually checking if a user is authorized
to make a certain call is a task for the backend.

### 2.5 Icons
We use the default Material Design Icons from Vuetify.
The icons are viewable at https://vuetifyjs.com/en/features/icon-fonts/#material-design-icons and https://materialdesignicons.com.

An mdi-icon is always prefixed with `mdi-` followed by the icon name:
```html
<v-icon>mdi-gitlab</v-icon>
```

## 3. Running on production
The Portal project utilizes [GitLab's CI/CD](https://gitlab.com/spaceleiden/portal/-/blob/develop/.gitlab-ci.yml). Development happens on the `develop` git branch. Our production environment uses the `main` git branch. CI builds on the `develop` branch automatically get deployed to a GitLab runner on `portal.testing.spaceleiden.nl`.

Builds on the `main` branch get deployed to our production `portal.spaceleiden.nl` environment.

For more information check out a recent [build](https://gitlab.com/spaceleiden/portal/-/pipelines), our [.gitlab-ci.yml file](https://gitlab.com/spaceleiden/portal/-/blob/develop/.gitlab-ci.yml) or GitLab's documentation on [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) and [GitLab runners](https://docs.gitlab.com/runner/).

## 4. License
Copyright &copy; Stichting The Space Leiden 2021.
This project is open source and licensed under the [MIT license](LICENSE).

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
