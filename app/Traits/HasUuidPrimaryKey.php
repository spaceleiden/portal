<?php

declare(strict_types=1);

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait HasUuidPrimaryKey
{
    public function getKeyType(): string
    {
        return 'string';
    }

    public function getIncrementing(): bool
    {
        return false;
    }

    protected static function bootHasUuidPrimaryKey()
    {
        // When the model is created we want to ensure the UUID is set
        self::creating(function (Model $model) {
            $keyName = $model->getKeyName();

            // If the UUID is empty we need to generate a new one
            // else we leave it alone as it already exists
            if (empty($model->{$keyName})) {
                $model->{$keyName} = Str::uuid()->toString();
            }
        });
    }
}
