<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('add_products');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required', 'min:3'],
            'description' => ['min:0'],
            'amount' => ['numeric', 'min:0', 'max:1000'],
            'enabled' => ['boolean'],
            'stock' => ['numeric', 'min:0', 'max:1000'],
            'currency' => ['in:EUR'],
        ];
    }

    // TEMP: gives us access to the validator error (but only the first)
    protected function failedValidation(Validator $validator)
    {
        $message = $validator->errors()->all();
        throw new ValidationException($message[0]);
    }
}
