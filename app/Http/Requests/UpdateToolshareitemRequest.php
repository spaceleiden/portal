<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateToolshareitemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasrole('member');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required', 'min:4'],
            'description' => ['required', 'min:4'],
            'offer' => ['required', 'in:Te Leen,Te Koop'],
            'objective' => ['required', 'in:Gezocht,Aangeboden'],
            'category' => ['required', 'in:Apparaat,Machine,Gereedschap,Materiaal,Overige'],
            'tool_type' => ['required', 'min:4'],
            'reachability' => ['required', 'min:4'],
            'visible' => ['required', 'boolean'],
            'responsibility' => ['required', 'accepted'],
        ];
    }
}
