<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class StoreToolshareitemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasrole('member');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['bail', 'required', 'min:4'],
            'description' => ['required', 'min:4'],
            'offer' => ['required', 'in:Te Leen,Te Koop'],
            'objective' => ['required', 'in:Gezocht,Aangeboden'],
            'category' => ['required', 'in:Apparaat,Machine,Gereedschap,Materiaal,Overige'],
            'tool_type' => ['required', 'min:4'],
            'reachability' => ['required', 'min:4'],
            'visible' => ['required', 'boolean'],
            'responsibility' => ['required', 'accepted'],
        ];
    }

    // TEMP: gives us access to the validator error (but only the first)
    protected function failedValidation(Validator $validator)
    {
        $message = $validator->errors()->all();
        throw new ValidationException($message[0]);
    }
}
