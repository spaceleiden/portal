<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdateActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('edit_activities');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['bail', 'required', 'min:3'],
            'activity_start' => ['date'],
            'description' => ['min:10'],
            'location' => ['min:3'],
            'type' => ['in:Talk,Workshop,Projects'],
            'speaker' => ['min:2', 'max:100'],
            'max_participants' => ['min:0'],
            'time' => [],
        ];
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'max_participants' => min($this->max_participants, 1000),
            'type' => $this->type ?? 'Workshop',
            'activity_start' => date('Y-m-d H:i:s', strtotime($this->activity_start . ' ' . $this->time . ':00')),
        ]);
    }

    // TEMP: gives us access to the validator error (but only the first)
    protected function failedValidation(Validator $validator)
    {
        $message = $validator->errors()->all();
        throw new ValidationException($message[0]);
    }
}
