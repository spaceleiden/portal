<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class StoreRobotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('add_robots');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        # TODO: Laravel 10 has lowercase and alpha:ascii
        return [
            'name' => ['bail', 'required', 'min:3', 'unique:robots', 'alpha_dash'],
            'enabled' => ['boolean'],
        ];
    }

    // TEMP: gives us access to the validator error (but only the first)
    protected function failedValidation(Validator $validator)
    {
        $message = $validator->errors()->all();
        throw new ValidationException($message[0]);
    }
}
