<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\Robot;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthenticateRobot
{
    public function handle(Request $request, Closure $next)
    {
        $bearer_token = $request->bearerToken();
        if (! $bearer_token || ! str_contains($bearer_token, ':')) {
            return response()->statusError(400, __('auth.unauthenticated'));
        }

        [$name, $token] = explode(':', $bearer_token);

        $robot = Robot::where('name', $name)->first();

        if (! $robot) {
            return response()->statusError(403, __('auth.failed'));
        }

        if (! Hash::check($token, $robot->token)) {
            return response()->statusError(403, __('auth.failed'));
        }

        if (! $robot->enabled) {
            return response()->statusError(403, __('auth.disabled'));
        }

        return $next($request);
    }
}
