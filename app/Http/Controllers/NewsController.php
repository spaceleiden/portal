<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Support\Facades\Http;

class NewsController extends Controller
{
    /**
     * Get five latest news items
     */
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->limit(5)->get();
        return response()->success(__('responses.success'), [
            'news' => $news->toArray(),
        ]);
    }

    public function blogposts()
    {
        $feed = Http::get('https://spaceleiden.nl/feed.xml');

        if ($feed->status() !== 200) {
            return response()->statusError(503, 'Blogposts zijn even niet beschikbaar, sorry!');
        }

        $xml = simplexml_load_string($feed->body());
        $news = [];
        $i = 0;
        foreach ($xml->entry as $post) {
            $news[] = [
                'id' => $i,
                'title' => (string) $post->title,
                'date' => (string) $post->published,
                'content' => (string) $post->summary,
                'image' => (string) $post->children('media', true)->thumbnail->attributes()->url,
                'url' => (string) $post->link->attributes()->href,
                'author' => (string) $post->author->name,
            ];
            $i++;
        }

        return response()->success(__('responses.success'), [
            'news' => $news,
        ]);
    }
}
