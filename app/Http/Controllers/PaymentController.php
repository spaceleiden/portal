<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Product;
use App\Models\User;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function balance(Request $request)
    {
        $user = $this->_user($request);

        return response()->success(__('responses.success'), [
            'balance' => $user->balance,
        ]);
    }

    public function index(Request $request)
    {
        $user = $this->_user($request);

        return response()->success(__('responses.success'), [
            'local_payments' => $user->local_payments->load('product:id,name'),
        ]);
    }

    public function create(Request $request)
    {
        $product_id = $request->input('product_id');
        if (! $product_id) {
            return response()->statusError(400, __('validation.invalid'));
        }

        $product = Product::find($product_id);

        if (! $product) {
            return response()->statusError(400, __('responses.finance.payment.product_not_found'));
        }

        $user = $this->_user($request);

        // TODO: deduplicate with a service
        if (! $product->enabled) {
            return response()->statusError(410, __('responses.finance.payment.create.product_not_for_sale'));
        }

        if ($product->stock < 1) {
            return response()->statusError(410, __('responses.finance.payment.create.out_of_stock'));
        }

        if ((float) $product->amount > (float) $user->balance) {
            return response()->statusError(409, __('responses.finance.payment.create.balance_too_low'));
        }

        $product->update([
            'stock' => $product->stock - 1,
        ]);

        $payment = new Payment();
        $payment->fill([
            'currency' => $product->currency,
            'amount' => $product->amount,
            'product_id' => $product->id,
            'user_id' => $user->id,
        ])->save();

        $user = $user->fresh();

        return response()->success(__('responses.success'), [
            'balance' => $user->balance,
            'payment' => $payment,
        ]);
    }

    private function _user(Request $request)
    {
        // If the route starts with robot_* it means
        // the current route was validated using the robot middleware.
        if ($request->routeIs('robot_*')) {
            $user_id = $request->input('user_id');
            if (! $user_id) {
                return response()->statusError(400, __('validation.invalid'));
            }
            $user = User::find($user_id);
        } else {
            $user = $request->user();
        }

        return $user;
    }
}
