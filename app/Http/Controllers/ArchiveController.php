<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ArchiveController extends Controller
{
    public function __construct()
    {
        $this->disk = Storage::disk('azure');
    }

    /**
     * Return a list of all directories and files inside the given path.
     * @throws ServiceException
     */
    public function list(Request $request): JsonResponse
    {
        $path = $request->path;
        $directories = $this->directorify($this->disk->directories($path));
        $files = $this->fileify($this->disk->files($path));
        return response()->success('Bestanden gevonden.', [
            'directories' => $directories,
            'files' => $files,
        ]);
    }

    /**
     * Retrieve the file contents for the given path
     * and serve it to the user as a blob.
     * @throws ServiceException
     */
    public function get(Request $request): JsonResponse|StreamedResponse
    {
        $path = $request->path;
        if (! $this->disk->exists($path)) {
            return response()->statusError(404, 'Bestand bestaat niet in het archief.');
        }

        return $this->disk->response($path);
    }

    /**
     * Return an array consisting of full
     * paths to directories and their basename.
     */
    private function directorify(array $originalDirectories): array
    {
        $directories = [];
        foreach ($originalDirectories as $directory) {
            $directories[] = [
                'path' => $directory,
                'name' => basename($directory),
            ];
        }
        return $directories;
    }

    /**
     * Return an array of files with their
     * basenames and full paths. Cleans out
     * the .folder file.
     */
    private function fileify(array $originalFiles): array
    {
        $files = [];
        foreach ($originalFiles as $file) {
            if (basename($file) === '.folder') {
                continue;
            }
            $files[] = [
                'path' => $file,
                'name' => basename($file),
                'size' => $this->disk->size($file),
                'updated_at' => $this->disk->lastModified($file),
                'content_type' => $this->disk->mimetype($file),
            ];
        }
        return $files;
    }
}
