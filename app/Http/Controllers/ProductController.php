<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Product;

use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        // we want to sort the products in the sales terminal by most popular
        // and take into account the products we have in stock most
        // so we define weights to each of these attributes, which can be changed for a better balance
        $popularityWeight = 0.7;
        $availabilityWeight = 0.3;
        $timeDecayFactor = 0.05;  // closer to 0 gives more importance to more recent sales

        // get all the products, with the datetime of their latest sale
        $products = Product::where('enabled', '1')
            ->orderBy('stock', 'desc')
            ->with([
                'allSales' => function ($query) {
                    $query->max('created_at');
                },
            ])
            ->get();

        // Calculate scores with time decay (so recent sales are more important)
        foreach ($products as &$product) {
            if (count($product->allSales) > 0) {
                $lastSaleDate = Carbon::parse($product->allSales[0]['created_at']);
                $daysSinceLastSale = Carbon::now()->diffInDays($lastSaleDate);
                $recentPopularity = $product['sales'] * exp(-$timeDecayFactor * $daysSinceLastSale);
                $product['score'] = ($popularityWeight * $recentPopularity) + ($availabilityWeight * $product['stock']);
            } else {
                // if there are no sales on the product, only use the current stock for the ordering score
                $product['score'] = ($availabilityWeight * $product['stock']);
            }
            // we don't need to send the rest of the sales data to our frontend
            unset($product->allSales);
            unset($product->sales);
        }

        // Sort the list based on scores in descending order
        $products = $products->toArray();
        usort($products, function ($a, $b) {
            return $b['score'] <=> $a['score'];
        });

        return response()->success(__('responses.success'), [
            'products' => $products,
        ]);
    }
}
