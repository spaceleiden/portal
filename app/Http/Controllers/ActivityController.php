<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Activity;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /**
     * Get all past and future activities, split on start datetime
     *
     * might want to add a $count parameter to be able to get the last and next $count activities
     */
    public function index(Request $request)
    {
        $future_activities = Activity::where('activity_start', '>=', now())
            ->orderBy('activity_start', 'asc')
            ->get();
        $future_activities = $future_activities->forget('registrations');

        $future_activities->each(function ($activity) {
            $activity->makeHidden('registrations');
        });

        return response()->success(__('responses.success'), [
            'future_activities' => $future_activities->toArray(),
        ]);
    }
}
