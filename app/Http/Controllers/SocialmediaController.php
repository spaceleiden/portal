<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Socialmedia;

class SocialmediaController extends Controller
{
    public function index()
    {
        $socialmedia = Socialmedia::all();
        return response()->success(__('responses.success'), [
            'socialmedia' => $socialmedia->toArray(),
        ]);
    }
}
