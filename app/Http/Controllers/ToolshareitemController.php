<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreToolshareitemRequest;
use App\Http\Requests\UpdateToolshareitemRequest;
use App\Models\Toolshareitem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ToolshareitemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user_id = $request->user()->toArray()['id'];
        $items = Toolshareitem::where('visible', '=', true)
            ->orWhere('user_id', '=', $user_id)
            ->orderBy('visible', 'desc')
            ->with('user')
            ->get();

        foreach ($items as $item) {
            $item['current_user_owns'] = $item->user->id === $user_id;
            $item['owner_first_name'] = $item->user->first_name;
            unset($item['user']);
            unset($item['user_id']);
        }

        // get all unique tool types for the page filters
        $tool_types = Toolshareitem::select('tool_type', 'objective')
            ->where('visible', '=', true)
            ->orWhere('user_id', '=', $user_id)
            ->distinct()->get();

        return response()->success(__('responses.success'), [
            'items' => $items->toArray(),
            'tool_types' => $tool_types->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreToolshareitemRequest $request)
    {
        $validated = $request->validated();
        $validated['user_id'] = $request->user()->toArray()['id'];

        Toolshareitem::create($validated);

        return response()->success(__('responses.success'), [
            'validated' => $validated,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update(UpdateToolshareitemRequest $request, $id)
    {
        $user = $request->user();

        // only the owner of the item, and an admin can edit the item
        if ($id !== $user['id'] && ! $user->hasRole('admin')) {
            return response()->statusError(403, 'Dit item mag je niet bewerken.');
        }

        $validated = $request->validated();

        $item = Toolshareitem::find($id);
        $item->fill($validated);
        $item->save();

        return response()->success(__('responses.success'), [
            'item' => $item,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $user = $request->user();

        // only the owner of the item, and an admin can delete the item
        if ($id !== $user['id'] && ! $user->hasRole('admin')) {
            return response()->statusError(403, 'Dit item mag je niet verwijderen.');
        }

        Toolshareitem::destroy($id);

        return response()->success(__('responses.success'));
    }
}
