<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\AddRegistrationRequest;
use App\Models\Activity;
use App\Models\Registration;
use App\Models\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AddRegistrationRequest $request)
    {
        $validated = $request->validated();
        $validated['user_id'] = $request->user()->toArray()['id'];

        // TODO add check to see if there's a limited amount of spaces for this activity
        // and if that has been reached before this registrations comes in

        Registration::create($validated);

        return response()->success(__('responses.success'), [
            'validated' => $validated,
        ]);
    }

    public function destroy(Request $request)
    {
        // validate whether the user_id in the registration is the current user

        $registration = Registration::where('activity_id', $request['activity_id'])
            ->where('user_id', $request->user()->id);
        $registration->delete();
        return response()->success(__('responses.success'));
    }
}
