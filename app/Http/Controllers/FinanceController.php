<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\MollieCallbackRequest;
use App\Jobs\ProcessSubscriptionCancelation;
use App\Mail\NewUser;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Mollie\Laravel\Facades\Mollie;

class FinanceController extends Controller
{
    public function getDefaultSubscription()
    {
        $subscription = Subscription::getDefaultSubscription(true)->toArray();

        return response()->success(__('responses.success'), $subscription);
    }

    public function callback(MollieCallbackRequest $request)
    {
        $customer_id = $request->get('customerId');
        $user_id = $request->get('userId');

        // 1. check if customerId is from the given userId
        $user = User::find($user_id);
        if ($user->mollie_customer_id !== $customer_id) {
            return response()->statusError(401, __('validation.invalid'));
        }

        // 2. create a subscription
        return $this->createSubscription($request, $user);
    }

    public function webhook(Request $request)
    {
        // TODO: Register the webhook call from Mollie somewhere in the database.

        // TODO: receive webhook, check current status for payment or mandate,
        //       update accordingly.

        // DEBUG: SET ROLE subscriber adhv status

        // TODO: ping admins of change

        // Return 200 OK to prevent mollie starting exponential backoff.
        return response()->statusError(200, __('responses.not_implemented'));
    }

    /**
     * Immediately cancel a user's active subscription.
     */
    public function cancel(Request $request)
    {
        $user = Auth::user();

        // 1. Make sure we have something to cancel.
        if (! $user->subscription) {
            return response()->statusError(422, __('responses.finance.subscription.cancel.unable'));
        }

        // 2. Cancel the subscription at mollie.
        $customer = Mollie::api()->customers->get($user->mollie_customer_id);
        $subscription = $customer->cancelSubscription($user->subscription->id);

        // 3. Make sure the user doesn't receive any news mails anymore, as a courtesy.
        $user->opt_in_news = false;
        $user->save();

        // 4. Make sure the user loses it's subscriber role at the end of this month
        //    (in case the start_date is 'first day of next month')
        $default_subscription = Subscription::getDefaultSubscription();
        ProcessSubscriptionCancelation::dispatch($user)
            ->delay(Carbon::create($default_subscription->start_date . ' at 00:00'));

        return response()->success(__('responses.finance.subscription.cancel.success'));
    }

    /**
     * Immediately reactivate a user's subscription, assuming a valid mandate still exists.
     */
    public function reactivate(Request $request)
    {
        $user = Auth::user();
        if (! $user->mandate) {
            // TODO: Create a new mandate and send the user away for the first payment.
            //       For now this is not implemented.
            return response()->statusError(422, __('responses.finance.subscription.reactivate.no_mandate'));
        }

        if ($user->subscription) {
            return response()->statusError(422, __('responses.finance.subscription.reactivate.already_has_subscription'));
        }

        // TODO: re-add opt_in_news according to new user choice?

        return $this->createSubscription($request, $user);
    }

    private function createSubscription(Request $request, User $user)
    {
        $default_subscription = Subscription::getDefaultSubscription();

        // 1. check if the user has a valid mandate
        if (! $user->mandate) {
            return response()->statusError(402, __('responses.finance.subscription.create.no_mandate'));
            // NOTE: The frontend should show a button to reload the page, or start a new first payment
        }

        // 2. check if the customer does not already have a subscription
        $customer = Mollie::api()->customers()->get($user->mollie_customer_id);
        $existing_subscriptions = $customer->subscriptions();
        foreach ($existing_subscriptions as $existing_subscription) {
            if ($existing_subscription->description === $default_subscription->description) {
                if ($existing_subscription->status === 'active') {
                    return response()->success(__('responses.finance.subscription.create.already_active'));
                } elseif ($existing_subscription->status !== 'canceled') {
                    return response()->statusError(409, __('responses.finance.subscription.create.already_exists'));
                }
            }
        }

        // 3. Prepare the subscription.
        $start_date = Carbon::create($default_subscription->start_date)->format('Y-m-d');
        $subscription_array = [
            'amount' => [
                'currency' => $default_subscription->currency,
                'value' => $default_subscription->amount,
            ],
            'startDate' => $start_date,
            'interval' => $default_subscription->interval,
            'description' => $default_subscription->description,
        ];

        // only use a webhook if we're not in development mode, since Mollie
        // won't be able to reach our localhost and we're not using ngrok-like proxies.
        if (! empty(config('app.mollie_webhook'))) {
            $baseUrl = config('app.mollie_webhook');
            $subscription_array['webhookUrl'] = $baseUrl . '/api/payments/webhook?customerId=' . $customer->id . '&userId=' . $user->id;
        }

        // 4. Create the subscription.
        $subscription = $customer->createSubscription($subscription_array);

        // 5. Now that the user has completed the payment flow, we can give them a member role
        $user->assignRole('member');

        // 6. Queue an e-mail for the new user.
        Mail::to($user->email)->queue(new NewUser($user));

        return response()->success(__('responses.finance.subscription.create.success'));
    }
}
