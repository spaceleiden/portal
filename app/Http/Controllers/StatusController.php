<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Support\Setting\Settings;

use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function __construct(Settings $settings)
    {
        parent::__construct($settings);
    }

    public function index(Request $request)
    {
        $lastchange = $this->settings->updated_at('status_is_open')->timestamp;
        $open = $this->settings->get('status_is_open');
        $message = $this->settings->get('status_message');

        return response()->json([
            'api' => '0.13',
            'space' => 'The Space Leiden',
            'logo' => 'https://spaceleiden.nl/assets/img/logo.png',
            'url' => 'https://spaceleiden.nl/',
            'location' => [
                'address' => 'Bètaplein 28, 2321KS Leiden, The Netherlands',
                'lat' => 52.145807367875896,
                'lon' => 4.493707267634785,
            ],
            'contact' => [
                'email' => 'info@spaceleiden.nl',
                'phone' => '+31715690356',
                'twitter' => 'spaceleiden',
            ],
            'issue_report_channels' => ['email'],
            'state' => [
                'open' => $open,
                'lastchange' => $lastchange,
                'message' => $message,
            ],
        ]);
    }
}
