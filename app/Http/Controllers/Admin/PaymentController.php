<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Product;
use App\Models\User;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        $payments = Payment::orderBy('created_at', 'desc')->get();
        $payments->load('user:id,first_name,prefix,last_name')->load('product:id,name');

        return response()->success(__('responses.success'), [
            'payments' => $payments->toArray(),
        ]);
    }

    public function create(Request $request)
    {
        // TODO: deduplicate with a service.
        $product = Product::find($request->input('product_id'));
        $user = User::find($request->input('user_id'));

        if ($product->stock < 1) {
            return response()->statusError(409, __('responses.finance.payment.create.out_of_stock'));
        }

        // Admins can force a payment even if the balance would be insufficient.

        $product->update([
            'stock' => $product->stock - 1,
        ]);

        $payment = new Payment();
        $payment->fill([
            'currency' => $product->currency,
            'amount' => $product->amount,
            'product_id' => $product->id,
            'user_id' => $user->id,
        ])->save();

        return response()->success(__('responses.success'), [
            'payment' => $payment,
        ]);
    }

    public function delete(Request $request, Payment $payment)
    {
        $payment->delete();
        return response()->success(__('responses.success'));
    }
}
