<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewsRequest;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewsRequest $request, News $news)
    {
        $validated = $request->validated();
        $validated['user_id'] = $request->user()->toArray()['id'];

        if ($news) {
            $news->fill($validated)->save();
        } else {
            News::create($validated);
        }

        return response()->success(__('responses.success'), [
            'validated' => $validated,
        ]);
    }

    public function remove(Request $request, News $news)
    {
        $news->delete();
        return response()->success(__('responses.admin.news.removed'));
    }
}
