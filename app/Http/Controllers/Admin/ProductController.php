<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Models\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::orderBy('created_at', 'desc')->get();

        return response()->success(__('responses.success'), [
            'products' => $products->toArray(),
        ]);
    }

    public function create(StoreProductRequest $request)
    {
        $validated = $request->validated();
        $product = new Product();
        $product->fill($validated);
        $product->save();
        return response()->success(__('responses.success'), [
            'product' => $product->toArray(),
        ]);
    }

    public function update(StoreProductRequest $request, Product $product)
    {
        $validated = $request->validated();
        $product->fill($validated);
        $product->save();
        return response()->success(__('responses.success'), [
            'product' => $product->toArray(),
        ]);
    }

    public function delete(Request $request, Product $product)
    {
        $product->delete();
        return response()->success(__('responses.success'));
    }
}
