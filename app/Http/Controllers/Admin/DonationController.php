<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Donation;

use Illuminate\Http\Request;

class DonationController extends Controller
{
    public function index(Request $request)
    {
        $donations = Donation::orderBy('created_at', 'desc')->get();
        $donations->load('user:id,first_name,prefix,last_name');

        return response()->success(__('responses.success'), [
            'donations' => $donations->toArray(),
        ]);
    }

    public function update(Request $request, Donation $donation)
    {
        $donation->add_to_balance = $request->input('add_to_balance');
        $donation->save();
        return response()->success(__('responses.success'));
    }
}
