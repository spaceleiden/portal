<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreActivityRequest;
use App\Http\Requests\UpdateActivityRequest;
use App\Models\Activity;
use App\Models\News;
use App\Models\Registration;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /**
     * Get all past and future activities, split on start datetime
     *
     * might want to add a $count parameter to be able to get the last and next $count activities
     */
    public function index(Request $request)
    {
        $past_activities = Activity::with('registrations')->with('attendees')->where('activity_start', '<', now())->orderBy('activity_start', 'desc')->get();
        $future_activities = Activity::with('registrations')->with('attendees')->where('activity_start', '>=', now())->orderBy('activity_start', 'asc')->get();
        // would it be faster to split this
        // - in the query or
        // - to collect all first and then split in php or
        // - to pass this to frontend and split in js?
        return response()->success(__('responses.success'), [
            'past_activities' => $past_activities->toArray(),
            'future_activities' => $future_activities->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreActivityRequest $request)
    {
        $validated = $request->validated();

        Activity::create($validated);

        if (isset($request['news']) && $request['news']) {
            $news = [
                'title' => $validated['title'],
                'content' => $validated['description'],
                'user_id' => $request->user()->id,
            ];
            News::create($news);
        }

        // TODO add a call to google calendar api to also automatically add this activity to our google calendar
        // perhaps make a setting for secret/"internal only" events, if we even want those

        // TODO maybe make a setting for activities to have an external registration form (e.g. offerzen's workshop)
        // Then a user registration could automatically lead them to an external url

        return response()->success(__('responses.success'), [
            'validated' => $validated,
        ]);
    }

    /**
     * Update an existing resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateActivityRequest $request, $id)
    {
        $validated = $request->validated();

        $activity = Activity::find($id);
        $activity->fill($validated);
        $activity->save();

        // TODO add a call to google calendar api to also automatically UPDATE this activity in our google calendar
        // perhaps make a setting for secret/"internal only" events, if we even want those

        return response()->success(__('responses.success'), [
            'activity' => $activity,
        ]);
    }

    public function remove(Request $request, Activity $activity)
    {
        $activity->delete();
        return response()->success(__('responses.admin.activities.removed'));
    }

    public function registrationCount()
    {
        $activities = Activity::select('activity_start', 'title')->withCount('registrations')->orderBy('activity_start', 'desc')->get();
        return response()->success(__('responses.success'), [
            'activities' => $activities,
        ]);
    }
}
