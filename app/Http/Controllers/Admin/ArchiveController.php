<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;

class ArchiveController extends Controller
{
    public function __construct()
    {
        $this->disk = Storage::disk('azure');
    }

    /**
     * Create a folder with the given path.
     * @throws ServiceException
     */
    public function createFolder(Request $request): JsonResponse
    {
        // Only create if path does not exist yet.
        if ($this->disk->exists($request->path . '/.folder')) {
            return response()->statusError(422, 'Folder bestaat al!');
        }

        // Storage::makeDirectory does not work for Azure,
        // so we create a new hidden file with the folder name in its path.
        $this->disk->put($request->path . '/.folder', '');
        return response()->success('Folder is gemaakt.');
    }

    /**
     * Create a new file.
     * @throws ServiceException
     */
    public function createFile(Request $request): JsonResponse
    {
        $file = $request->file('file');

        // Only if final path does not exist yet, prevent overwrites.
        if ($this->disk->exists($request->path . '/' . $file->getClientOriginalName())) {
            return response()->statusError(422, 'Dat betstand bestaat al!');
        }

        $path = $this->disk->putFileAs($request->path, $file, $file->getClientOriginalName());
        return response()->success('Bestand is geupload', [
            'path' => $path,
        ]);
    }

    /**
     * Delete the given folder path.
     * @throws ServiceException
     */
    public function deleteFolder(Request $request): JsonResponse
    {
        // Does the path exist?
        if (! $this->disk->exists($request->path . '/.folder')) {
            return response()->statusError(422, 'Die folder bestaat niet!');
        }

        $childFileCount = sizeof($this->allFiles($this->disk->allFiles($request->path)));
        $childFolderCount = sizeof($this->disk->allDirectories($request->path));
        if ($childFileCount > 0 || $childFolderCount > 0) {
            return response()->statusError(422, 'Folder is niet leeg!');
        }

        // For some reason the deleteDirectory function _does_ work,
        // while the makeDirectory function does not work for Azure.
        $this->disk->deleteDirectory($request->path);
        return response()->success('Folder is verwijderd.');
    }

    /**
     * Delete the given file.
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\JsonResponse
     * @throws ServiceException
     */
    public function deleteFile(Request $request): JsonResponse
    {
        // Catch exception if path does not exist
        if (! $this->disk->exists($request->path)) {
            return response()->statusError(422, 'Dat bestand bestaat niet!');
        }

        $this->disk->delete($request->path);
        return response()->success('Bestand is verwijderd.');
    }

    /**
     * Filter the .folder file out of an array of files.
     */
    private function allFiles($originalFiles): array
    {
        $files = [];
        foreach ($originalFiles as $file) {
            if (basename($file) !== '.folder') {
                $files[] = $file;
            }
        }
        return $files;
    }
}
