<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRobotRequest;

use App\Models\Robot;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RobotController extends Controller
{
    public function index(Request $request)
    {
        $robots = Robot::get()->load('user:id,first_name,last_name,prefix');
        return response()->success(__('responses.success'), [
            'robots' => $robots,
        ]);
    }

    public function create(StoreRobotRequest $request)
    {
        $validated = $request->validated();

        $token = 'rt_' . Str::random(64);

        $robot = new Robot();
        $robot->fill([
            'name' => $validated['name'],
            'token' => Hash::make($token),
            'enabled' => true,
            'created_by' => $request->user()->id,
        ]);
        $robot->save();

        return response()->success(__('responses.success'), [
            'robot' => $robot,
            'token' => $token,
        ]);
    }

    public function update(Request $request, Robot $robot)
    {
        $robot->enabled = (bool) $request->input('enabled');
        $robot->save();

        return response()->success(__('responses.success'), [
            'robot' => $robot,
        ]);
    }

    public function delete(Request $request, Robot $robot)
    {
        $robot->delete();
        return response()->success(__('responses.success'));
    }
}
