<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon;
use Illuminate\Http\Request;
use Mollie\Laravel\Facades\Mollie;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::orderBy('created_at', 'desc')->get();

        return response()->success(__('responses.success'), [
            'users' => $users->toArray(),
        ]);
    }

    public function birthdays(Request $request)
    {
        $users = User::select('birthdate', 'first_name', 'prefix', 'last_name')
            ->whereNotNull('birthdate')
            ->whereMonth('birthdate', '>=', Carbon\Carbon::now())
            ->whereDay('birthdate', '>=', Carbon\Carbon::now())
            ->orWhereMonth('birthdate', '=', Carbon\Carbon::now()->addMonth())
            ->orWhereMonth('birthdate', '=', Carbon\Carbon::now()->addMonths(2))
            ->get();

        $now = Carbon\Carbon::now();
        foreach ($users as $user) {
            // create a carbon date from the birthdate with format m-d
            $birthday = Carbon\Carbon::createFromDate($user->birthdate)->format('m-d');
            $birthday = Carbon\Carbon::createFromFormat('m-d', $birthday);

            // set it to next occurrence
            if ($now >= $birthday) {
                $birthday->modify('next year');
            }

            // add to the collection item
            $user['birthday'] = $birthday;
        }

        // sort the users based on next birthday
        $users = $users->toArray();
        usort($users, function ($a, $b) {
            return $a['birthday']->diffInDays($b['birthday']);
        });

        return response()->success(__('responses.success'), [
            'users' => $users,
        ]);
    }

    public function finance(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);
        return response()->success(__('responses.success'), [
            'mandate' => $user->mandate,
            'subscription' => $user->subscription,
        ]);
    }

    public function payments(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);
        $customer = Mollie::api()->customers()->get($user->mollie_customer_id);
        $payments = $customer->payments();
        return response()->success(__('responses.success'), [
            'payments' => $payments,
        ]);
    }
}
