<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Mail\NewDonation;
use App\Models\Donation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mollie\Laravel\Facades\Mollie;

class DonationController extends Controller
{
    /**
     * One-off member donation request.
     * Sure, I'll take your money!
     *
     * @return response
     */
    public function createOneTimeMemberDonation(Request $request, $amount, $add_to_balance)
    {
        $user = $request->user();

        // Create payment and send it to the frontend
        $payment_href = $this->createDonationPayment($amount, $user->mollie_customer_id, $user->id, $add_to_balance);

        if (! $payment_href) {
            return response()->statusError(422, __('responses.finance.payment.amount_too_small'));
        }

        return response()->success(__('responses.finance.payment.ready'), [
            'href' => $payment_href,
        ]);
    }

    public function callbackDonationPayment(Request $request)
    {
        $customer_id = $request->get('customerId');
        $payments = Mollie::api()->customers->get($customer_id)->payments();

        // This is not the best way to determine if a donation came through,
        // however the real validation happens in the webhook callback function
        // (webhookDonationPayment), so this is just a function for UX-sake.

        foreach ($payments as $payment) {
            // Only process the most recent donation payment, which is the
            // payment on 'top' of the payments array.
            if (! str_contains($payment->description, 'Eenmalige Donatie')) {
                continue;
            }
            if ($payment->status !== 'paid') {
                return response()->statusError(402, __('responses.finance.payment.not_paid'));
            }
            return response()->success(__('responses.finance.payment.thanks'));
        }
        return response()->statusError(402, 'Geen recente donaties gevonden, probeer het opnieuw.');
    }

    public function webhookDonationPayment(Request $request)
    {
        // https://docs.mollie.com/overview/webhooks
        $payment = Mollie::api()->payments()->get($request->input('id'));
        $user = User::where('mollie_customer_id', $payment->customerId)->first();
        if ($payment->status !== 'paid' || ! str_contains($payment->description, 'Eenmalige Donatie')) {
            return response()->statusError(409, 'Transaction is not yet paid or is not a one-off donation.');
        }

        // Return if the payment_id already exists in the donation table.
        if (Donation::where('payment_id', $payment->id)->first()) {
            return response()->statusError(400, 'Transaction is already registered as a donation.');
        }

        // Send mail to donor to confirm donation.
        Mail::to($user->email)->queue(new NewDonation($user, $payment));

        // Register the donation in the database.
        $donation = new Donation();
        $donation->fill([
            'amount' => $payment->amount->value,
            'currency' => $payment->amount->currency,
            'add_to_balance' => $request->get('add_to_balance'),
            'user_id' => $user->id,
            'payment_id' => $payment->id,
        ]);

        if (! $donation->save()) {
            return response()->statusError(500, 'Could not save donation in database.');
        }

        return response()->success('Donation added to database, mail sent.');
    }

    private function createDonationPayment($amount, $customer_id, $user_id = false, $add_to_balance = 0, $currency = 'EUR')
    {
        $baseUrl = config('app.url');
        $redirectUrl = $baseUrl . '/donate/callback?customerId=' . $customer_id . '&userId=' . $user_id;
        $amount_formatted = number_format((float) $amount, 2);

        if ($amount_formatted < 1.00) {
            return false;
        }

        $payment_data = [
            'amount' => [
                'currency' => $currency,
                'value' => $amount_formatted,
            ],
            'sequenceType' => 'oneoff',
            'description' => 'Eenmalige Donatie aan Stichting The Space Leiden',
            'redirectUrl' => $redirectUrl,
        ];

        if ($customer_id !== false) {
            $payment_data['customerId'] = $customer_id;
        }

        // only use a webhook if we're not in development mode, since Mollie
        // won't be able to reach our localhost and we're not using ngrok-like proxies.
        if (! empty(config('app.mollie_webhook'))) {
            $payment_data['webhookUrl'] = $baseUrl . '/api/payments/donate/webhook?add_to_balance=' . $add_to_balance;
        }

        $payment = Mollie::api()->payments()->create($payment_data);
        return $payment->_links->checkout->href;
    }
}
