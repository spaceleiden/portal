<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    /**
     * Handle a login request.
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->validated();

        /** @var User $user */
        $user = User::where('email', $credentials['email'])->first();

        if (! $user || ! Hash::check($credentials['password'], $user->password)) {
            return response()->statusError(401, __('auth.failed'));
        }

        return response()->success(__('auth.success'), [
            'token' => $user->createToken('auth-token')->plainTextToken,
        ]);
    }

    /**
     * Handle a forgot password request.
     */
    public function forgot(ForgotRequest $request)
    {
        $status = Password::sendResetLink(
            $request->only('email')
        );

        // Return identical response for success and invalid user responses to
        // prevent user enumeration.
        if ($status === Password::RESET_LINK_SENT || $status === Password::INVALID_USER) {
            return response()->success(__($status));
        }
        return response()->statusError(400, __($status));
    }

    /**
     * Handle a password reset request.
     */
    public function reset(ResetPasswordRequest $request)
    {
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {

                // NOTE: To prevent hashing a hash, we 'store'
                // the password as plaintext here, but before
                // that actually happens, the User::setPasswordAttribute
                // will hash the given password before storing it to disk.
                $user->forceFill([
                    'password' => $password,
                ]);

                // Revoke any active api tokens the current user may have.
                $user->tokens()->delete();
                $user->save();
                event(new PasswordReset($user));
            }
        );

        if ($status === Password::PASSWORD_RESET) {
            return response()->success(__($status));
        }
        return response()->statusError(400, __($status));
    }
}
