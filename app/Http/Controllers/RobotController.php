<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Jobs\InvalidateCardLinkCode;
use App\Models\CardLinkCode;

use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Arr;

class RobotController extends Controller
{
    public function get(Request $request, $card_uid)
    {
        $user = User::where('card_uid', $card_uid)->first();

        if (! $user) {
            return response()->statusError(404, __('responses.cards.unknown'));
        }

        // TODO: add pin code maybe?

        return response()->success(__('responses.success'), [
            'first_name' => $user->first_name,
            'balance' => $user->balance,
            'user_id' => $user->id,
        ]);
    }

    public function create_link_code(Request $request, $card_uid)
    {
        $user = User::where('card_uid', $card_uid)->first();

        // Ensure the card is not already used by someone else.
        if ($user) {
            return response()->statusError(400, __('responses.cards.taken'));
        }

        // Ensure there is not already a card link code for this card uid.
        // If there is, we return that existing code.
        $existing_card = CardLinkCode::where('card_uid', $card_uid)->first();
        if ($existing_card) {
            return response()->success(__('responses.cards.code_exists_for_card'), [
                'link_code' => $existing_card->link_code,
                'ttl' => 'unknown',
            ]);
        }

        // Create a new unique card link code for the card_uid.
        $card_link_code = new CardLinkCode();
        $card_link_code->fill([
            'link_code' => $this->generateUniqueCode(),
            'card_uid' => $card_uid,
        ])->save();

        if (! $card_link_code) {
            return response()->statusError(500, __('responses.cards.failed'));
        }

        // Dispatch a job to delete the card code in 10 minutes.
        $ttl = now()->addMinutes(10);
        InvalidateCardLinkCode::dispatch($card_link_code)
            ->delay($ttl);

        return response()->success(__('responses.cards.code_created'), [
            'link_code' => $card_link_code->link_code,
            'ttl' => $ttl,
        ]);
    }

    private function generateUniqueCode()
    {
        $chars = [...range('A', 'Z'), ...range(0, 9)];
        $code = '';

        for ($i = 0; $i < 5; $i++) {
            $code = $code . Arr::random($chars);
        }

        if (CardLinkCode::where('link_code', $code)->first()) {
            # :O this code already exists!
            return $this->generateUniqueCode();
        }

        return $code;
    }
}
