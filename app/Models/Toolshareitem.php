<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Toolshareitem extends Model
{
    use HasFactory;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'offer',
        'objective',
        'category',
        'tool_type',
        'reachability',
        'visible',
        'user_id',
    ];

    /**
     * Get the user offering the item.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
