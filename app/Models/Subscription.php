<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //use HasFactory;

    protected $fillable = [
        'description',
        'mandate_title',
        'amount',
        'currency',
        'interval',
        'default',
    ];

    protected $casts = [
        'default' => 'boolean',
    ];

    /**
     * Return the currently default subscription.
     *
     * @param bool $minimal Only return minimal information for signup page.
     */
    public static function getDefaultSubscription($minimal = false): self
    {
        if ($minimal) {
            return self::select(['amount', 'currency'])->firstWhere('default', true);
        }

        return self::firstWhere('default', true);
    }
}
