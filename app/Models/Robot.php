<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\HasUuidPrimaryKey;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Robot extends Model
{
    use HasFactory;

    use HasUuidPrimaryKey;

    protected $fillable = [
        'name',
        'enabled',
        'token',
        'created_by',
    ];

    protected $hidden = [
        'token',
    ];

    protected $casts = [
        'enabled' => 'boolean',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
