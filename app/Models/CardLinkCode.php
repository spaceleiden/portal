<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\HasUuidPrimaryKey;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardLinkCode extends Model
{
    use HasFactory;

    use HasUuidPrimaryKey;

    protected $fillable = [
        'link_code',
        'card_uid',
    ];
}
