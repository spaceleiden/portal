<?php

declare(strict_types=1);

namespace App\Models;

use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Traits\HasUuidPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Mollie\Laravel\Facades\Mollie;
use Propaganistas\LaravelPhone\Casts\E164PhoneNumberCast;

use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory;

    use Notifiable;

    use HasUuidPrimaryKey;

    use HasApiTokens;

    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'prefix',
        'last_name',
        'email',
        'password',
        'phone_number',
        'postal_code',
        'birthdate',
        'opt_in_news',
        'discord_name',
        'card_uid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name',
        'role_names',
        'balance',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birthdate' => 'date',
        'opt_in_news' => 'boolean',
        'phone_number' => E164PhoneNumberCast::class . ':AUTO,NL',
    ];

    /**
     * Get the user's full name.
     *
     * @var string
     */
    public function getFullNameAttribute(): string
    {
        return str_replace('  ', ' ', "{$this->first_name} {$this->prefix} {$this->last_name}");
    }

    public function getRoleNamesAttribute(): Collection
    {
        return $this->roles()->pluck('name');
    }

    /**
     * Make sure the password is hashed before it is stored.
     */
    public function setPasswordAttribute($password): string
    {
        return $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Get a valid mandate from the user.
     */
    public function getMandateAttribute()
    {
        $customer = Mollie::api()->customers()->get($this->mollie_customer_id);
        $mandates = $customer->mandates();

        foreach ($mandates as $mandate) {
            if ($mandate->status === 'valid') {
                return $mandate;
            }
        }
        return false;
    }

    public function getSubscriptionAttribute()
    {
        $customer = Mollie::api()->customers()->get($this->mollie_customer_id);
        $subscriptions = $customer->subscriptions();

        foreach ($subscriptions as $subscription) {
            // TODO: return list of active suscriptions,
            // or just the membership subscription!
            if ($subscription->status === 'active') {
                return $subscription;
            }
        }
        return false;
    }

    public function getPaymentsAttribute()
    {
        if (! $this->mollie_customer_id) {
            return [];
        }
        $customer = Mollie::api()->customers()->get($this->mollie_customer_id);
        $payments = $customer->payments();
        if (! $payments) {
            return [];
        }
        return (array) $payments;
    }

    /**
     * Check if the current user has a valid mandate.
     */
    public function hasValidMandate(): bool
    {
        $customer = Mollie::api()->customers()->get($this->mollie_customer_id);
        $mandates = $customer->mandates();

        foreach ($mandates as $mandate) {
            if ($mandate->status === 'valid') {
                return true;
            }
        }
        return false;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get all of the donations for the User
     */
    public function donations(): HasMany
    {
        return $this->hasMany(Donation::class);
    }

    /**
     * Get all of the local payments for the User
     */
    public function local_payments(): HasMany
    {
        return $this->hasMany(Payment::class)->oldest();
    }

    /**
     * Returns the balance for the User
     *
     * This function is not currency aware.
     *
     * @return float
     */
    public function getBalanceAttribute()
    {
        $balance = 0.00;

        foreach ($this->donations as $donation) {
            if ($donation->add_to_balance) {
                $balance += (float) $donation->amount;
            }
        }

        foreach ($this->local_payments as $payment) {
            $balance -= (float) $payment->amount;
        }

        return number_format($balance, 2);
    }
}
