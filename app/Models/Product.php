<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\HasUuidPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;

    use HasUuidPrimaryKey;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'amount',
        'enabled',
        'stock',
        'currency',
    ];

    protected $appends = [
        'sales',
    ];

    protected $casts = [
        'enabled' => 'boolean',
    ];

    // get the count of the sales on the product
    public function getSalesAttribute(): int
    {
        return $this->hasMany(Payment::class)->count();
    }

    // get all sales on the relevant product
    public function allSales(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Payment::class);
    }
}
