<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\HasUuidPrimaryKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Donation extends Model
{
    use HasFactory;

    use HasUuidPrimaryKey;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'currency',
        'add_to_balance',
        'user_id',
        'payment_id',
    ];

    protected $casts = [
        'add_to_balance' => 'boolean',
    ];

    /**
     * Get the user that owns the Donation
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
