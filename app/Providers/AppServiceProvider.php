<?php

declare(strict_types=1);

namespace App\Providers;

use App\Models\Setting;
use App\Support\Setting\Settings;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(Settings::class, function () {
            return new Settings(Setting::all());
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
