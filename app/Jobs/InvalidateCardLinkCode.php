<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\CardLinkCode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class InvalidateCardLinkCode implements ShouldQueue
{
    use Dispatchable;

    use InteractsWithQueue;

    use Queueable;

    use SerializesModels;

    /**
     * The CardLinkCode instance.
     *
     * @var \App\Models\CardLinkCode
     */
    protected $card_link_code;

    /**
     * Create a new job instance.
     *
     * @param App\Models\CardLinkCode $card_link_code
     */
    public function __construct(CardLinkCode $card_link_code)
    {
        $this->card_link_code = $card_link_code;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {

        // TODO: This doesn't seem to work when using the database queue driver. Sync driver does work.
        $clc = $this->card_link_code;
        Log::info('Deleting CardLinkCode', [$clc]);
        $clc->delete();
    }
}
