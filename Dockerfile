################################
# gitlab.com/spaceleiden/portal
#


##########
# Stage 1
#

FROM node:16-alpine as frontend-builder
WORKDIR /app

# Copy package manager files, and artisan because 
# that way laravel-mix knows that it's laravel.
COPY webpack.mix.js package.json package-lock.json ./
COPY resources resources/

RUN npm install
RUN npm run prod


##########
# Stage 2
#

FROM composer:2 as backend-builder
WORKDIR /app

COPY composer.json composer.lock ./
RUN composer install \
    --ignore-platform-reqs \
    --no-ansi \
    --no-autoloader \
    --no-dev \
    --no-interaction \
    --no-scripts

COPY . .
RUN composer dump-autoload -a --ignore-platform-reqs


##########
# Stage 3
#

FROM registry.gitlab.com/spaceleiden/portal/php-laravel-apache:2023.3.9

ADD .docker/apache.conf /etc/apache2/sites-available/000-default.conf
ADD .docker/entrypoint.sh /usr/local/bin/entrypoint.sh

WORKDIR /srv
COPY --from=backend-builder /app .
COPY --from=frontend-builder /app/public ./public

RUN chgrp -R www-data ./storage ./bootstrap/cache && chmod -R 770 ./storage ./bootstrap/cache

RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
CMD ["apache2-foreground"]
