<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        foreach (['Cola', 'Fanta', 'Club Mate', 'Icetea', 'Radler', 'Pils'] as $product) {
            DB::table('products')->insert([
                'id' => Str::uuid()->toString(),
                'name' => $product,
                'description' => '',
                'currency' => 'EUR',
                'amount' => '0.50',
                'stock' => 24,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
