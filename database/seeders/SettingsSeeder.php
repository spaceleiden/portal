<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    public function run()
    {
        Setting::firstOrCreate([
            'name' => 'status_is_open',
            'value' => true,
            'type' => 'boolean',
        ]);
        Setting::firstOrCreate([
            'name' => 'status_message',
            'value' => '<strong>We zijn fysiek weer open!</strong> &mdash; Iedere donderdag zijn wij vanaf 19:00 open. Wel is <a href="https://forms.gle/U3PCpQFYDNeN97Cv5">inschrijven</a> verplicht (vol=vol) en hebben we nog <a href="https://spaceleiden.nl/covid-19">maatregelen</a>. Kom je ook gezellig langs? <a href="https://spaceleiden.nl/opening">Klik hier voor meer informatie</a> <br>',
        ]);
    }
}
