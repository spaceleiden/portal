<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Create 10 dummy users
        User::factory(10)->create();

        // Create a regular user
        $doe = User::factory()->create([
            'email' => 'user@spaceleiden.nl',
        ]);

        // Create an admin user
        $admin = User::factory()->create([
            'email' => 'admin@spaceleiden.nl',
        ]);
        $admin->assignRole('admin');
    }
}
