<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $products = Product::all();
        $users = User::all();
        foreach ([0, 2, 8] as $i) {
            DB::table('payments')->insert([
                'id' => Str::uuid()->toString(),
                'currency' => $products[0]->currency,
                'amount' => $products[0]->amount,
                'product_id' => $products[0]->id,
                'user_id' => $users[$i]->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
