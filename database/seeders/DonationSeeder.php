<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;

class DonationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $users = User::all();

        foreach ([0, 2, 8] as $i) {
            DB::table('donations')->insert([
                'id' => Str::uuid()->toString(),
                'payment_id' => 'tr_1234567890',
                'currency' => 'EUR',
                'amount' => '5.00',
                'add_to_balance' => true,
                'user_id' => $users[$i]->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
