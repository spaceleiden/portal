<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Toolshareitem;
use App\Models\User;
use Illuminate\Database\Seeder;

class ToolshareitemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $admin = User::select('id')->where('email', '=', 'admin@spaceleiden.nl')->get()->toArray()[0];
        $user = User::select('id')->where('email', '=', 'user@spaceleiden.nl')->get()->toArray()[0];

        Toolshareitem::factory(10)->create();

        // create some visible and one invisible item owned by test admin
        Toolshareitem::factory(2)->create([
            'user_id' => $admin['id'],
            'visible' => 1,
        ]);
        Toolshareitem::factory()->create([
            'user_id' => $admin['id'],
            'visible' => 0,
        ]);

        // create some visible and one invisible item owned by test user
        Toolshareitem::factory(2)->create([
            'user_id' => $user['id'],
            'visible' => 1,
        ]);
        Toolshareitem::factory()->create([
            'user_id' => $user['id'],
            'visible' => 0,
        ]);
    }
}
