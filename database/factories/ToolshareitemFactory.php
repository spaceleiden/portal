<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Toolshareitem;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ToolshareitemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Toolshareitem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::all();

        $categories = [
            'Apparaat',
            'Machine',
            'Gereedschap',
        ];

        return [
            'name' => $this->faker->word . ' X' . $this->faker->randomNumber(1) . '000',
            'description' => $this->faker->text(),
            'offer' => $this->faker->randomElement(['Te Leen', 'Te Koop']),
            'objective' => $this->faker->randomElement(['Gezocht', 'Aangeboden']),
            'tool_type' => $this->faker->word,
            'category' => $this->faker->randomElement($categories),
            'reachability' => 'Via discord: ' . $this->faker->userName() . '#1234',
            'visible' => (int) (rand(0, 1) + 0.6),
            'user_id' => $users[rand(0, 11)]['id'],
        ];
    }
}
