<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'throttled' => 'Please wait before retrying.',
    'token' => 'This password reset token is invalid.',

    // Identical messages to prevent user enumeration.
    'sent' => "If the email address is correct you'll receive an email shortly.",
    'user' => "If the email address is correct you'll receive an email shortly.",

];
