<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API call responses.
    |--------------------------------------------------------------------------
    |
    */

    'success' => 'The request was successful.',
    'welcome' => 'Welcome back!',
    'failed' => 'The request was not successful.',
    'not_implemented' => 'This request is not implemented yet.',

    'finance' => [
        'mandate' => [
            'ready' => 'Mandate payment has been created and is ready.',
        ],
        'subscription' => [
            'cancel' => [
                'unable' => 'Nothing to cancel, user has no valid subscription.',
                'success' => 'Subscription is cancelled.',
            ],
            'reactivate' => [
                'no_mandate' => 'User has no mandate, unable to reactivate.',
                'already_has_subscription' => 'Active subscription already exists.',
            ],
            'create' => [
                'no_mandate' => 'No valid mandate exists (yet).',
                'already_active' => 'Subscription is already activated.',
                'already_exists' => 'Subscription already exists and is not active or canceled.',
                'success' => 'Subscription has been activated.',
            ]
        ],
        'payment' => [
            'create' => [
                'balance_too_low' => 'Balance is too low to purchase this product.',
                'out_of_stock' => 'Product is out of stock.',
                'product_not_found' => 'Product does not exist.',
                'product_not_for_sale' => 'Product is currently not for sale.',
            ],
        ],
    ],

    'cards' => [
        'unknown' => 'This card is unknown to us.',
        'code_exists_for_card' => 'Link code has already been created for this card.',
        'code_created' => 'Link code has been created for your card.',
        'invalid_link_code' => 'Link code is not valid.',
        'register_success' => 'Card has been linked to your account.',
        'no_card_registered' => 'No card is linked to your account.',
        'taken' => 'Card is already registered to someone.',
    ],

];
