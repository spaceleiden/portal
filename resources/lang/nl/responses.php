<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API call responses.
    |--------------------------------------------------------------------------
    |
    */

    'success' => 'De aanvraag is gelukt.',
    'welcome' => 'Welkom terug!',
    'failed' => 'De aanvraag is niet gelukt.',
    'not_implemented' => 'Deze functie is nog niet geimplementeerd.',

    'finance' => [
        'mandate' => [
            'ready' => 'Mandaatbetaling is aangemaakt en klaar om betaald te worden.',
        ],
        'payment' => [
            'ready' => 'Betaling is aangemaakt en klaar om betaald te worden',
            'amount_too_small' => 'Het bedrag is te laag.',
            'not_paid' => 'De betaling is zo te zien nog niet gelukt.',
            'thanks' => 'Bedankt voor je betaling!',
        ],
        'subscription' => [
            'cancel' => [
                'unable' => 'Kan deelnemerschap niet annuleren, gebruiker heeft geen deelnemerschap.',
                'success' => 'Deelnemerschap is geannuleerd.',
            ],
            'reactivate' => [
                'no_mandate' => 'Gebruiker heeft geen mandaat, kan deelnemerschap niet heractiveren.',
                'already_has_subscription' => 'Een actief deelnemerschap bestaat al.',
            ],
            'create' => [
                'no_mandate' => 'Gebruiker heeft (nog) geen geldig mandaat gegeven.',
                'already_active' => 'Deelnemerschap is al geactiveerd.',
                'already_exists' => 'Deelnemerschap bestaat al, maar is niet actief of geannuleerd.',
                'success' => 'Deelnemerschap is geactiveerd.',
            ]
        ],
        'payment' => [
            'create' => [
                'balance_too_low' => 'Balans is te laag om product te kopen.',
                'out_of_stock' => 'Product is niet meer op voorraad.',
                'product_not_found' => 'Product bestaat niet.',
                'product_not_for_sale' => 'Product momenteel niet te koop.',
            ],
        ],
    ],

    'admin' => [
        'news' => [
            'created' => 'Het nieuwsartikel is aangemaakt.',
            'removed' => 'Het nieuwsartikel is verwijderd.',
            'updated' => 'Het nieuwsartikel is bijgewerkt.',
        ],
        'activities' => [
            'removed' => 'De activiteit is verwijderd.',
        ],
        'registraties' => [
            'double' => 'Deze deelnemer is al geregistreerd voor deze activiteit.'
        ]
    ],

];
