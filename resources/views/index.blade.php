<!DOCTYPE html>

<html lang="nl">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>The Space Portal</title>
        <meta name="description" content="Deelnemersportaal van The Space Leiden">
        <meta name="author" content="The Space Leiden">

        <link rel="shortcut icon" href="https://spaceleiden.nl/favicon.ico">

        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        <div id="app"></div>
    </body>
    <script type="application/javascript" src="/js/app.js"></script>
</html>
