@component('mail::message')
Bedankt voor je donatie, **{{ $name }}**!

Met jouw donatie kunnen we onze visie van een hackerspace en makerspace in Leiden voortzetten.

@component('mail::panel')
Je hebt **{{ $donation_amount }} {{ $donation_currency }}** gedoneerd 
aan Stichting The Space Leiden.
@endcomponent

Mocht je vragen hebben, stuur ons dan een [mailtje](mailto:bestuur@spaceleiden.nl)
of spreek iemand met een rood vest aan op de space.

Tot volgende donderdag,<br>
Team The Space Leiden
@endcomponent
